# Goal of the Challenge

Check your skills in designing test scenarios and programming, which are necessary for automating the tests.

To do this, it is necessary to design scenarios for two WhatsApp features and to solve exercise 09 of the CodeKata (http://codekata.com/kata/kata09-back-to-the-checkout/), which can also be found here in Kata09.md.

### Must contain: ###

* Specifying two WhatsApp features (it does not matter if the app is Android, iOS, Windows Phone, or Web version) in the format that's convenient for you;
* Each specification must contain at least one test scenario;
* Response of exercise 09 of the CodeKata (http://codekata.com/kata/kata09-back-to-the-checkout/), or you can find it here at Kata09.md. You have the freedom to do in the programming language that is convenient to you;

### Earn more points if it contains: ###

* Specification made in Gherkin format (Cucumber, BDD);
* CodeKata exercise response in any programming language;


### **Submission Process** ###
The candidate should implement the solution and send a pull request to this repository with the solution.

The Pull Request process works as follows:

1. Candidate will fork this repository (will not clone directly!)

2. It will make your project on that fork.

3. You will commit and upload the changes to YOUR fork.

4. Through the Bitbucket interface, you will send a Pull Request.


### **Good Luck!** ###